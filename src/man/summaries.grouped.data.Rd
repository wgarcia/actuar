\name{summaries.grouped.data}
\alias{summaries.grouped.data}
\alias{mean.grouped.data}
\alias{var.grouped.data}
\alias{sd.grouped.data}
\title{Summaries of Grouped Data}
\description{
  Summaries for \link[base]{class} \code{"grouped data"} objects.

  \code{mean} computes the sample mean of grouped data objects.

  \code{var} computes the sample variance of grouped data objects.

  \code{sd} computes the sample standard deviation of grouped data objects.
}
\usage{
\method{mean}{grouped.data}(x, \dots)
var.grouped.data(x, \dots)
sd.grouped.data(x, \dots)
}
\arguments{
  \item{x}{an object of class \code{"grouped.data"}.}
  \item{\dots}{further arguments passed to or from other methods.}
}
\details{
  \code{mean} computes the mean of grouped data with group boundaries
  \eqn{c_1, \dots,  c_r}{c[1], \dots, c[r]} and group frequencies
  \eqn{n_1, \dots,     n_r}{n[1], \dots, n[r]} is   \deqn{\sum_{j = 1}^r
  \frac{c_{j - 1} + c_j}{2}\, n_j.}{sum(j; (c[j - 1] + c[j])/2 * n[j]).}

  \code{var} computes the sample variance of grouped data with group boundaries \eqn{c_1,
    \dots, c_r}{c[1], \dots, c[r]} and group frequencies \eqn{n_1,
    \dots, n_r}{n[1], \dots, n[r]} which is
  \deqn{\sum_{j = 1}^r \frac{n_j ( n (c_{j-1} + c_j) - \sum_{k=1}^r {(c_{k - 1} + c_k} ))}{4 n^2 (n-1)},}{%
        sum(j; (n[j] * ( n * (c[j-1] + c[j]) -  sum(k; (c[k - 1] +
	c[k]))/(4 * n^2 * (n-1)),}
  where \eqn{n = \sum_{j = 1}^r n_j}{n = sum(j; n[j])}.

  \code{sd} computes the sample sample standard deviation  of grouped
  data with group
  boundaries \eqn{c_1, \dots, c_r}{c[1], \dots, c[r]} and group
  frequencies \eqn{n_1, \dots, n_r}{n[1], \dots, n[r]} which is the squared
  root of the sample variance.

  The sample variance for grouped data differs from the variance
  computed from the empirical raw moments because of two reasons. First,
  it takes into account the degrees of freedom. Second, it
  underestimates the true total variation within the group classes. The
  difference between the sample and the true variance is called the
  Shepherd's correction factor, and assuming a uniform distribution for
  the data within intervals it is approximately equal to the square of
  the class size divided by 12.

}
\value{
  A named vector of summaries.
}
\seealso{
  \code{\link{grouped.data}} to create grouped data objects;
  \code{\link{emm}} to compute higher moments.
}
\references{
  Klugman, S. A., Panjer, H. H. and Willmot, G. E. (1998),
  \emph{Loss Models, From Data to Decisions}, Wiley.
}
\author{
  Vincent Goulet \email{vincent.goulet@act.ulaval.ca}
  Walter Garcia-Fontes \email{walter.garcia@upf.edu}
}
\examples{
data(gdental)
mean(gdental)
var(gdental)
sd(gdental)
cj <- c(0,1,2,3,4)
nj <- c(1,5, 3, 2)
data <- grouped.data(Group=cj,Frequency=nj)
(sum(nj)-1)/sum(nj)*var.grouped.data(data)+1/12
emm(data,2)-emm(data)^2
}
\keyword{univar}
